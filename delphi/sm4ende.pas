unit sm4ende;

interface
{
  根据C语言版翻译的代码，
  只用到了ecb加密，bcb的没有写
  在delphi2006下测试正常
  --qmcode 20200928
}
uses
  SysUtils;
type
  TBytes16 = array[0..15] of Byte;

  SM4_Context = record
     mode: Integer;
     sk: array[0..31] of Cardinal;
  end;

  PSM4_Context = ^SM4_Context;

//密钥长度固定为16字节
procedure sm4_setkey_enc(ctx: PSM4_Context; key: TBytes16);
procedure sm4_setkey_dec(ctx: PSM4_Context; key: TBytes16);

//加密内容的长度必需是16的倍数,密文长度和明文长度相同
procedure sm4_crypt_ecb(ctx: PSM4_Context;len: Integer;input,output: PByte);
implementation

const SM4_ENCRYPT: integer = 1;
const SM4_DECRYPT: integer = 0;

type
  TMaxBytes = array[0..MaxInt shr 16] of Byte;

  PMaxBytes = ^TMaxBytes;

  Cardinal32 = array[0..31] of Cardinal;

  PCardinal32 = ^Cardinal32;

  PBytes16 = ^TBytes16;
//
///*
// * Expanded SM4 S-boxes
// /* Sbox table: 8bits input convert to 8 bits output*/

const SboxTable: array[0..15,0..15] of Byte =(
($d6,$90,$e9,$fe,$cc,$e1,$3d,$b7,$16,$b6,$14,$c2,$28,$fb,$2c,$05),
($2b,$67,$9a,$76,$2a,$be,$04,$c3,$aa,$44,$13,$26,$49,$86,$06,$99),
($9c,$42,$50,$f4,$91,$ef,$98,$7a,$33,$54,$0b,$43,$ed,$cf,$ac,$62),
($e4,$b3,$1c,$a9,$c9,$08,$e8,$95,$80,$df,$94,$fa,$75,$8f,$3f,$a6),
($47,$07,$a7,$fc,$f3,$73,$17,$ba,$83,$59,$3c,$19,$e6,$85,$4f,$a8),
($68,$6b,$81,$b2,$71,$64,$da,$8b,$f8,$eb,$0f,$4b,$70,$56,$9d,$35),
($1e,$24,$0e,$5e,$63,$58,$d1,$a2,$25,$22,$7c,$3b,$01,$21,$78,$87),
($d4,$00,$46,$57,$9f,$d3,$27,$52,$4c,$36,$02,$e7,$a0,$c4,$c8,$9e),
($ea,$bf,$8a,$d2,$40,$c7,$38,$b5,$a3,$f7,$f2,$ce,$f9,$61,$15,$a1),
($e0,$ae,$5d,$a4,$9b,$34,$1a,$55,$ad,$93,$32,$30,$f5,$8c,$b1,$e3),
($1d,$f6,$e2,$2e,$82,$66,$ca,$60,$c0,$29,$23,$ab,$0d,$53,$4e,$6f),
($d5,$db,$37,$45,$de,$fd,$8e,$2f,$03,$ff,$6a,$72,$6d,$6c,$5b,$51),
($8d,$1b,$af,$92,$bb,$dd,$bc,$7f,$11,$d9,$5c,$41,$1f,$10,$5a,$d8),
($0a,$c1,$31,$88,$a5,$cd,$7b,$bd,$2d,$74,$d0,$12,$b8,$e5,$b4,$b0),
($89,$69,$97,$4a,$0c,$96,$77,$7e,$65,$b9,$f1,$09,$c5,$6e,$c6,$84),
($18,$f0,$7d,$ec,$3a,$dc,$4d,$20,$79,$ee,$5f,$3e,$d7,$cb,$39,$48)
);


///* System parameter */
const FK: array[0..3] of Cardinal = ($a3b1bac6,$56aa3350,$677d9197,$b27022dc);


///* fixed parameter */
const CK: array[0..31] of Cardinal =
(
$00070e15,$1c232a31,$383f464d,$545b6269,
$70777e85,$8c939aa1,$a8afb6bd,$c4cbd2d9,
$e0e7eef5,$fc030a11,$181f262d,$343b4249,
$50575e65,$6c737a81,$888f969d,$a4abb2b9,
$c0c7ced5,$dce3eaf1,$f8ff060d,$141b2229,
$30373e45,$4c535a61,$686f767d,$848b9299,
$a0a7aeb5,$bcc3cad1,$d8dfe6ed,$f4fb0209,
$10171e25,$2c333a41,$484f565d,$646b7279
);


function ROTL(x,n: Cardinal): Cardinal;
begin
  Result := ((x and $FFFFFFFF) shl n) or (x shr (32 - n));
end;

function PrintCardinalHex(v: Cardinal): string;
begin
  Result := Format('%s-%s-%s-%s',[
  IntToHex((v and $FF000000) shr 24,2),
  IntToHex((v and $FF0000) shr 16,2),
  IntToHex((v and $FF00) shr 8,2),
  IntToHex((v and $FF),2)])
end;

function GET_ULONG_BE(b: PByte;i: Integer): Cardinal;
begin
  {$IFDEF  __DEBUG}
    Writeln(Format('GET_ULONG_BE: %s-%s-%s-%s',[
    IntToHex(b^,2),
    IntToHex(PByte(Cardinal(b) + 1)^,2),
    IntToHex(PByte(Cardinal(b) + 2)^,2),
    IntToHex(PByte(Cardinal(b) + 3)^,2)]));
  {$ENDIF}
  Result :=  PMaxBytes(b)[i] shl 24
  or
  PMaxBytes(b)[i + 1] shl 16
  or
  PMaxBytes(b)[i + 2] shl 8
  or
  PMaxBytes(b)[i + 3];
  {$IFDEF  __DEBUG}

    Writeln(PrintCardinalHex(Result));

  {$ENDIF}
end;


procedure PUT_ULONG_BE(n: Cardinal;b: PByte;i: Integer);
begin
  PMaxBytes(b)[i] := n shr 24;
  PMaxBytes(b)[i + 1] := n shr 16;
  PMaxBytes(b)[i + 2] := n shr 8;
  PMaxBytes(b)[i + 3] := n;
end;
///*
// * private function:
// * look up in SboxTable and get the related value.
// * args:    [in] inch: 0x00~0xFF (8 bits unsigned value).
// */
function sm4Sbox(inch: Byte): Byte;
var
  pTable: PMaxBytes;
begin
  pTable := @SboxTable;

  Result := pTable[inch]
end;


function sm4Lt(ka: Cardinal): Cardinal;
var
  bb: Cardinal;
  c: Cardinal;
  a: array[0..3] of Byte;
  b: array[0..3] of Byte;
begin
  PUT_ULONG_BE(ka,@a,0);

  b[0] := sm4Sbox(a[0]);
  b[1] := sm4Sbox(a[1]);
  b[2] := sm4Sbox(a[2]);
  b[3] := sm4Sbox(a[3]);

  bb := GET_ULONG_BE(@b,0);

  c := bb xor ROTL(bb, 2) xor ROTL(bb, 10) xor ROTL(bb, 18) xor ROTL(bb, 24); 
  Result := c;
end;



function sm4F(x0,x1,x2,x3,rk: Cardinal): Cardinal;
begin
  Result := x0 xor sm4Lt(x1 xor x2 xor x3 xor rk); 
end;


procedure sm4_one_round(sk: PCardinal32; input,output: PBytes16);
var
  i: Cardinal;
  ulbuf: array[0..35] of Cardinal;
begin
  FillChar(ulbuf,SizeOf(ulbuf),0);
  ulbuf[0] := GET_ULONG_BE(PByte(input),0);
  ulbuf[1] := GET_ULONG_BE(PByte(input),4);
  ulbuf[2] := GET_ULONG_BE(PByte(input),8);
  ulbuf[3] := GET_ULONG_BE(PByte(input),12);

  i :=0;
  while(i < 32) do
  begin
    ulbuf[i+4] := sm4F(ulbuf[i], ulbuf[i+1], ulbuf[i+2], ulbuf[i+3], sk[i]);
    inc(i);
  end;

  PUT_ULONG_BE(ulbuf[35],PByte(output),0);
  PUT_ULONG_BE(ulbuf[34],PByte(output),4);
  PUT_ULONG_BE(ulbuf[33],PByte(output),8);
  PUT_ULONG_BE(ulbuf[32],PByte(output),12);

end;


 
function sm4CalciRK(ka: Cardinal): Cardinal;
var
  bb: Cardinal;
  a: array[0..3] of Byte;
  b: array[0..3] of Byte;
begin
  PUT_ULONG_BE(ka,@a,0);

  b[0] := sm4Sbox(a[0]);
  b[1] := sm4Sbox(a[1]);
  b[2] := sm4Sbox(a[2]);
  b[3] := sm4Sbox(a[3]);


  bb := GET_ULONG_BE(@b,0);

  Result := bb xor (ROTL(bb,13)) xor ROTL(bb,23);

end;

procedure sm4_setkey(SK: PCardinal32;key: PByte);
var
  MK: array[0..3] of Cardinal;
  k: array[0..35] of Cardinal;
  i: Cardinal;
begin
  i := 0;
  MK[0] := GET_ULONG_BE(key,0);
  MK[1] := GET_ULONG_BE(key,4);
  MK[2] := GET_ULONG_BE(key,8);
  MK[3] := GET_ULONG_BE(key,12);


  k[0] := MK[0] xor FK[0];
  k[1] := MK[1] xor FK[1];
  k[2] := MK[2] xor FK[2];
  k[3] := MK[3] xor FK[3];

  while(i < 32) do
  begin
    k[i+4] := k[i] xor (sm4CalciRK(k[i+1] xor k[i+2] xor k[i+3] xor CK[i]));
    SK[i] := k[i+4];
    Inc(i);
  end;
 
end;

procedure sm4_setkey_enc(ctx: PSM4_Context; key: TBytes16);
begin
  ctx.mode := SM4_ENCRYPT;
  sm4_setkey(@ctx.sk,@key);
end;
procedure sm4_setkey_dec(ctx: PSM4_Context; key: TBytes16);
var
  I: Integer;
  c: Cardinal;
begin
  ctx.mode := SM4_ENCRYPT;
  sm4_setkey( @ctx.sk, pbyte(@key) );
  for I := 0 to 15  do
  begin
    c := ctx.sk[i];
    ctx.sk[i] := ctx.sk[31-i];
    ctx.sk[31-i]:= c;
  end;

end;

procedure sm4_crypt_ecb(ctx: PSM4_Context;len: Integer;input,output: PByte);
begin
  if len mod 16 <> 0 then
    raise Exception.Create('数据长度必需是16的整数倍');
    
  while len >= 16 do
  begin
    sm4_one_round(@ctx.sk, PBytes16(input), PBytes16(output) );
    Inc(input,16);
    Inc(output,16);
    len := len - 16;
  end;
end;

end.
