program SM4;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  sm4ende in 'sm4ende.pas';
var
  input: string;
  value: array[0..15] of Byte;
  encodeValue,decodeValue: array[0..15] of byte;
  ctx: SM4_Context;
  key: TBytes16;
  procedure PrintBytes(v: PByte;len: integer);
  begin
    while len > 0 do
    begin
      Write(IntToHex(v^,2));
      Write('-');
      Dec(len);
      Inc(v);
    end;
    Write(#10#13);    
  end;
begin
  { TODO -oUser -cConsole Main : Insert code here }

  //明文
  Move('1234567812345678',value,16);

  //密钥
  Move('ABCDE,FGHIJ,KLMNO,',key,16);

  FillChar(encodeValue,Length(encodeValue),0);
  FillChar(decodeValue,Length(decodeValue),0);

  sm4_setkey_enc(@ctx,key);
  
  sm4_crypt_ecb(@ctx,Length(value),@value,@encodeValue);

  sm4_setkey_dec(@ctx,key);

  sm4_crypt_ecb(@ctx,Length(encodeValue),@encodeValue,@decodeValue);

  Writeln('明文:');
  PrintBytes(@value,Length(value));
  Writeln('密文:');
  PrintBytes(@encodeValue,Length(encodeValue));
  Writeln('解密:');
  PrintBytes(@decodeValue,Length(decodeValue));  
  Readln(input);
end.
