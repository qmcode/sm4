/*
 * SM4/SMS4 algorithm test programme
 * 2012-4-21
 */
 
#include <string.h>
#include <stdio.h>
#include <memory.h>
#include "sm4.h"
 
int main()
{
    unsigned char key[16] = "helloworld";
    //unsigned char input[16] = {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef,0xfe,0xdc,0xba,0x98,0x76,0x54,0x32,0x10};
	unsigned char input[] = "hello world";
	
	//加密数据长度为16的整数倍，如：1个字节加密结果为16字节，16个字节加密结果还是16个字节
	//17个字节加密结果为32个字节
    unsigned char output[64];
	
	//解密缓冲区长度同加密缓冲区，也需要为16的整数倍
	unsigned char decodeValue[64];
	
	
    sm4_context ctx;
    unsigned long i;
 
	printf("input:%s\n",key);
	
	printf("keysize:%d\n",sizeof(key));
	
	printf("input:%s\n",input);
	
	printf("inputsize:%d\n",sizeof(input));
	
	memset(output,0xff,sizeof(output));
	memset(decodeValue,0xff,sizeof(decodeValue));
	
	
    //encrypt standard testing vector
    sm4_setkey_enc(&ctx,key);
    sm4_crypt_ecb(&ctx,1,sizeof(input),input,output);
	

	printf("output:\n");
    for(i=0;i<sizeof(output);i++)
        printf("%02x ", output[i]);
	
    printf("\n");
 

    //decrypt testing
    sm4_setkey_dec(&ctx,key);
    sm4_crypt_ecb(&ctx,0,sizeof(input),output,decodeValue);
	
	printf("decodeValue:\n");
	
	for(i=0;i<sizeof(decodeValue);i++)
        printf("%02x ", decodeValue[i]);
	printf("\n");
	
	printf("retString:%s\n",decodeValue);
	
    return 0;
}